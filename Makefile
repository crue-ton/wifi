PREFIX?=/usr/local
BIN?=$(PREFIX)/bin

PROGRAM_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

CLANG := $(shell command -v clang++ 2>/dev/null)
GCC := $(shell command -v g++ 2>/dev/null)

default:
	@printf "targets:\n\tmake install\n\tmake uninstall\n\tmake purge\n"

install:
	cp src/config.def.h src/config.h
ifndef CLANG
	@printf "clang/clang++ not found!\nShould be prompted to use g++ instead...\n"
	@echo -n "Use g++ instead? [Y/n] "; \
	read _c; \
	if [ $${_c:-'N'} = 'y' ];then \
		g++ -Wall src/main.cpp src/lib/connection.cpp src/lib/extras.cpp src/lib/global.cpp -o src/nwifi; \
	fi
else
	clang++ -Wall src/main.cpp src/lib/connection.cpp src/lib/extras.cpp src/lib/global.cpp -o src/nwifi 
endif

	if [ -f $(BIN)/nwifi ];then rm -f $(BIN)/nwifi;fi
	ln -s $(PROGRAM_DIR)/src/nwifi $(BIN)/nwifi

uninstall:
	rm -f src/nwifi
	rm -f $(BIN)/nwifi

purge:
	rm -f $(BIN)/nwifi
	cd ..;rm -rf nwifi
