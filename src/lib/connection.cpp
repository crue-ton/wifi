// Functions used for connecting and disconnecting
#include <string>
#include <unistd.h>
#include <iostream>
#include <termios.h>
#include <algorithm>

#include "global.h"

void connect() {
  std::string ssid;
  std::string password;

  std::cout << "Enter WiFi SSID: ";
  std::getline(std::cin, ssid);
  ssid = "\""+ssid+"\"";

  // Hide password input
  termios oldt;
  tcgetattr(STDIN_FILENO, &oldt);
  termios newt = oldt;
  newt.c_lflag &= ~ECHO;
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);

  std::cout << "Enter Password: ";
  std::getline(std::cin, password);
  
  get_super();

  std::string cmd = *elv_cmd + " nmcli d wifi connect " + ssid;
  if (!password.empty()) cmd = cmd + " password " + password;

  if (*elv_cmd == "su -c") cmd = "\'" + cmd + "\'";

  system(cmd.c_str());
}

void disconnect() {
  get_super();
  std::cout << "A";
  std::string current_connection = exec("nmcli -t -f active,ssid dev wifi|grep '^yes:'");
  current_connection = current_connection.substr(current_connection.find("yes:") + 4);
  current_connection.erase(std::remove(current_connection.begin(),
                                       current_connection.end(), '\n'),
                                       current_connection.cend());

  std::string cmd = *elv_cmd + " nmcli con down id " + "\"" + current_connection + "\"";
  if (*elv_cmd == "su -c") cmd = "\'" + cmd + "\'";

  system(cmd.c_str());
}
