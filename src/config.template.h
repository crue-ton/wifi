#include <string>
#ifndef _CONFIG_H_
#define _CONFIG_H_
// Known Networks //
/*
 *
 * Leave Password field blank if there is no password for the connection
 *
*/

extern std::string website;
extern std::string knownNetworks[];
extern std::string networksPasswords[];

std::string website = "https://www.gnu.org";

std::string knownNetworks[] = {
  "CafeWiFi",
  "HomeWiFi"
};

std::string networksPasswords[] = {
  "",
  "complicated_password"
};
#endif
