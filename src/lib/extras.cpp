// Functions use to test your connection or get the current networks around you
#include <stdio.h>
#include <sstream>

#include <iostream>
#include "global.h"
#include "../config.h"

void test_wifi() {
  std::string cmd = "curl -s " + website + " >/dev/null";
  
  int status = system(cmd.c_str());
  if (status == 0) std::cout << "Connection is good!" << std::endl;
  else throw std::runtime_error("Connection seems to fail, check connections!\n");
}

void get_networks() {
  std::string networks = exec("nmcli -t -f active,ssid dev wifi");
  size_t pos = networks.find(":"); 
  networks.erase(0, pos);
  std::cout << networks << std::endl;
}

void help_message() {
  std::cout << "All Available Flags:" << std::endl;
  std::cout << "\t-t | Test WiFi Connection" << std::endl;
  std::cout << "\t-d | Disconnect from current WiFi connection" << std::endl;
  std::cout << "\t-c | Connect to a WiFi Network" << std::endl;
  std::cout << "\t-h | Display Help Message" << std::endl;
}

void no_args() {
  std::cout << "No arguments given!" << std::endl;
  std::cout << "Using known networks defined in config!" << std::endl;

  std::string choice;
  std::string cmd;

  size_t n = sizeof(knownNetworks)/sizeof(knownNetworks[0]);

  for (size_t i=0;i<n;i++) {
    std::cout << "(" << i+1 << ") " << knownNetworks[i] << std::endl;
  }

  std::cout << "> ";
  std::getline(std::cin, choice);
  std::stringstream _choice(choice);

  int k=0;
  _choice >> k;
  k -= 1;

  get_super();
  cmd = *elv_cmd + " nmcli d wifi connect " + knownNetworks[k];
  if (networksPasswords[k] == "") {
    std::cout << "Password Not Found For Network" << std::endl;
    std::cout << "Assuming Network Doesn't Need a Password" << std::endl;
  } else cmd = cmd + " password " + networksPasswords[k];

  system(cmd.c_str());
}
