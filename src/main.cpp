#include <string>
#include <sstream>
#include <iostream>
#include <unistd.h>
#include <termios.h>
#include <algorithm>

#include "lib/global.h"
#include "lib/extras.h"
#include "lib/connection.h"

int main(int argc, char * argv[]) {
  for (int i=0;i<argc;++i) {
    if (std::string(argv[i]) == "-t") test_wifi();
    else if (std::string(argv[i]) == "-d") disconnect();
    else if (std::string(argv[i]) == "-c") connect();
    else if (std::string(argv[i]) == "-g") get_networks();
    else if (std::string(argv[i]) == "-h") help_message();
    else if (2 > argc) no_args(); 
  }
}
