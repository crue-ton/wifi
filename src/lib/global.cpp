// Function that are used in multiple files and needed everywhere
// Example:
// The function get_super is used to whether the program needs to use sudo, doas, or `su -c`
#include <string>
#include <stdio.h>
#include <unistd.h>
#include <iostream>

#include "global.h"

std::string *elv_cmd = new std::string;
void get_super() {
  std::string *choice = nullptr;
  auto privs = geteuid();

  if (privs != 0) {
    std::cout << "Warning! Some flags require elevation!\n\n";

    if (!system("command -v sudo >/dev/null 2>&1") &&
        !system("command -v doas >/dev/null 2>&1")) {
      std::cout << "sudo and doas found on system..." << std::endl
                << "Please choose one!" << std::endl;
      std::cout << "[1] sudo\n [2] doas\n" << "[1-2]: ";

      std::getline(std::cin, *choice);

      if (*choice == "1") *elv_cmd = "sudo";
      else if (*choice == "2") *elv_cmd = "doas";
      else std::cout << "Not valid choice!";
    }
    else if (!system("command -v sudo >/dev/null 2>&1")) {*elv_cmd = "sudo";}
    else if (!system("command -v doas >/dev/null 2>&1")) {*elv_cmd = "doas";}
    else {
      std::cout << "sudo nor doas found!\n";
      std::cout << "falling back on `su -c`!\n";
      *elv_cmd = "su -c";
    }

  }
}

std::string exec(std::string command) {
  char buffer[128];
  std::string result = "";

  // Open pipe to file
  FILE * pipe = popen(command.c_str(), "r");
  if (!pipe) return "popen failed!";

  // read till end of process:
  while (!feof(pipe)) {
    // use buffer to read and add to result
    if (fgets(buffer, 128, pipe) != NULL) result += buffer;
  }

  pclose(pipe);
  return result;
}
